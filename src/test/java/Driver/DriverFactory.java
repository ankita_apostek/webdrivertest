package Driver;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

public class DriverFactory {

   public WebDriver driver;

    public DriverFactory(String platform, String browser){
        driver = createDriver(platform,browser);
    }

    private WebDriver createDriver(String platform, String browser) {
        if (browser.equalsIgnoreCase("chrome"))
        return createChromeDriver(platform);
        else if (browser.equalsIgnoreCase("firefox"))
            return  createFirefoxDriver(platform);
        else
            return null;
        
    }
    
//    private WebDriver createMobileWebDriver(String platform) {
//       return null;
//    }
    private WebDriver createFirefoxDriver(String platform) {
    	
    	System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir") + "\\src\\main\\resources\\Drivers\\geckodriver.exe" );
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setBrowserName("firefox");
		FirefoxOptions options = new FirefoxOptions();
			if (platform.equals("Mobile Web")) {
			
			options.addArguments(
					"--user-agent=Mozilla/5.0 (Linux; Android 6.0.1; SM-G920V Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36");
			System.out.println(options.toString());
			options.addArguments("start-minimized");
			
		}
			capabilities.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options);
			driver = new FirefoxDriver(capabilities);
	
		if (platform.equals("Mobile Web"))
			driver.manage().window().setSize(new Dimension(360, 680));
		else
			driver.manage().window().maximize();
		return driver;
   }

   private WebDriver createChromeDriver(String platform ) {
        WebDriver driver;
        System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "\\src\\main\\resources\\Drivers\\ChromeDriver-Win.exe" );
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//		System.setProperty("webdriver.chrome.driver", getChromeDriverPath(System.getProperty("os.name")));
		capabilities.setBrowserName("chrome");
		ChromeOptions options = new ChromeOptions();
		if (platform.equals("Mobile Web")) {
			options.addArguments(
					"--user-agent=Mozilla/5.0 (Linux; Android 7.0; SM-G892A Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/60.0.3112.107 Mobile Safari/537.36");
			System.out.println(options.toString());
			options.addArguments("start-minimized");
		}
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(capabilities);
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		//driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
		if (platform.equals("Mobile Web"))
			driver.manage().window().setSize(new Dimension(360, 680));
		else
			driver.manage().window().maximize();
		return driver;
    }
 }


