package com.Gmail.tests;

import Driver.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import PageObjects.AmazonMobileWebPO;
import PageObjects.AmazonPO;
import bsh.This;

import java.util.concurrent.TimeUnit;

public class AmazonMobileWebLogin extends DriverFactory {
	
	public WebDriver driver;
	public String url;
	public static Logger logger;
	public AmazonMobileWebPO po;

	public AmazonMobileWebLogin() {
		super("Mobile Web", "firefox");
		this.driver = super.driver;
	}

	@BeforeClass(alwaysRun = true)
	public void setup(){
		po = new AmazonMobileWebPO(driver);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		url = "https://www.amazon.com/gp/sign-in.html";
	}
	
	@AfterClass(alwaysRun = true)
	public void Close(){
		 driver.quit();
	 }
	
	@DataProvider
	public Object[][] loginData()
	{
		return new Object[][] {
			{"mishra.ankita@gmail.com","qwert123"},
			{"mishra.ankita4@gmail.com","Amazon123"}
		};
	}
	
	@Test(groups="p1", dataProvider= "loginData")
    public void login(String username, String password) {
		driver.get(url);
		po.doLoginAmazon(username,password);
		driver.get("https://www.amazon.com");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	     System.out.println(po.Mob_SignOut.getText());
	     if (po.Mob_SignOut.getText().equalsIgnoreCase("Sign out")) {
	    	 System.out.println("Login Successful");
	     }
	     else {
	    	 System.out.println("Login Unsuccessful");
	     }
	}
	
	@Test(groups="p1")
    public void Search() throws InterruptedException {
		Thread.sleep(3000);
		po.doSearchAmazon();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	     boolean test = po.Mob_SearchList.getText().contains("results");
	     Assert.assertEquals(test,true);
	     logger.info("Search Complete");
	}
	
	@Test(groups="p1")
	 public void SecondResult() {
		po.Mob_SecondItem.click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		po.Mob_AddingToCart.click();
		boolean cart = po.Mob_AddedCart.getText().contains("Proceed to checkout");
	    Assert.assertEquals(cart,true);
	    logger.info("Item added to cart");
	   	
	}
}
