package com.Gmail.tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Login_Test 
{
public WebDriver driver;

@BeforeClass(alwaysRun = true)
public void setup(){ 
	System.setProperty("webdriver.chrome.driver", "C:\\Users\\Ankita\\Downloads\\ChromeDriver-Win.exe");
	driver= new ChromeDriver();
		
}
 @AfterClass(alwaysRun = true)
 public void Close(){
	 driver.close();
 }

 @Test(groups= "p2")
public void Negative(){
	 String url = "https://accounts.google.com/signin";
     driver.get(url);
     WebElement email_phone = driver.findElement(By.xpath("//input[@id='identifierId']"));
     email_phone.sendKeys("anksanks70");
     driver.findElement(By.id("identifierNext")).click();
     try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
     password.sendKeys("Anksanks70");
     driver.findElement(By.id("passwordNext")).click();
     
	//WebElement wrong_pwd = driver.findElement(By.xpath("//*[@class='dEOOab RxsGPe']"));
   // for (WebElement WrongPwd : wrong_pwd) {
   //  System.out.println(wrong_pwd.getText()); //}
     
    WebElement wrong_pwd = driver.findElement(By.xpath("//*[@class='dEOOab RxsGPe']"));
    Assert.assertEquals(wrong_pwd.getText(), "Wrong password. Try again or click Forgot password to reset it.");
   
    
}
 
 @Test(groups="p1")
 public void Positive(){
	 String url = "https://accounts.google.com/signin";
     driver.get(url);
     WebElement email_phone = driver.findElement(By.xpath("//input[@id='identifierId']"));
     email_phone.sendKeys("anksanks70");
     driver.findElement(By.id("identifierNext")).click();
     try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
     password.sendKeys("Anksanks70$");
     driver.findElement(By.id("passwordNext")).click();
     }
 /*
  @Test(groups="p3")
 public void PositiveAgain(){
	 String url = "https://accounts.google.com/signin";
     driver.get(url);
     WebElement email_phone = driver.findElement(By.xpath("//input[@id='identifierId']"));
     email_phone.sendKeys("anksanks70");
     driver.findElement(By.id("identifierNext")).click();
     try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
     password.sendKeys("Anksanks70$");
     driver.findElement(By.id("passwordNext")).click();
     }   */
}
