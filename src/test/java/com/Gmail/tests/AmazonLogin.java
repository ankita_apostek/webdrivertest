package com.Gmail.tests;


import Driver.DriverFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import PageObjects.AmazonPO;
import bsh.This;

import java.util.concurrent.TimeUnit;

public class AmazonLogin extends DriverFactory {
	
	public WebDriver driver;
	public String url;
	public static Logger logger;
	public AmazonPO po;

	public AmazonLogin() {
		super("desktop", "firefox");
		this.driver = super.driver;
	}

	@BeforeClass(alwaysRun = true)
	public void setup(){
		po = new AmazonPO(driver);
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		url = "https://www.amazon.com/gp/sign-in.html";
	}
	
	@AfterClass(alwaysRun = true)
	public void Close(){
		 driver.quit();
	 }
	
	@DataProvider
	public Object[][] loginData()
	{
		return new Object[][] {
			{"mishra.ankita@gmail.com","qwert123"},
			{"mishra.ankita4@gmail.com","Amazon123"}
		};
	}
	
	@Test(groups="p1", dataProvider= "loginData")
    public void login(String username, String password) {
		driver.get(url);
		po.doLoginAmazon(username,password);
		driver.get("https://www.amazon.com");
		
	
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	     System.out.println(po.MyAccount.getText());
	     if (po.MyAccount.getText().equalsIgnoreCase("Hello, ankita")) {
	    	 System.out.println("Login Successful");
	     }
	     else {
	    	 System.out.println("Login Unsuccessful");
	     }
	}
	
	@Test(groups="p1")
    public void Search() throws InterruptedException {
		Thread.sleep(3000);
		po.doSearchAmazon();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	     boolean test = po.SearchList.getText().contains("results for \"Core Java books\"");
	     Assert.assertEquals(test,true);
	     logger.info("Search Complete");
	}
	
	@Test(groups="p1")
	 public void SecondResult() {
		po.SecondItem.click();
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		po.AddingToCart.click();
		boolean cart = po.AddCart.getText().contains("Proceed to checkout");
	    Assert.assertEquals(cart,true);
	    logger.info("Item added to cart");
	   	
	}
}
