package com.Gmail.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.log4testng.Logger;

import DataProviders.PhpTravelsDP;
import Driver.DriverFactory;
import PageObjects.PhpTravelsPO;


public class PhpTravels extends DriverFactory{
	public WebDriver driver;
	public String url;
	public static Logger logger;
	public PhpTravelsPO pto;

	public PhpTravels() {
		super("Desktop", "firefox");
		this.driver = super.driver;
	}

	@BeforeClass(alwaysRun = true)
	public void setup(){
		pto = new PhpTravelsPO(driver);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		url = "https://www.phptravels.net/login";
	}
	
	@AfterClass(alwaysRun = true)
	public void Close(){
		 driver.quit();
	 }
	
	@Test(groups="p1", dataProvider= "PhpLogin", dataProviderClass = PhpTravelsDP.class)
    public void login(String username, String password) {
		driver.get(url);
		pto.PhpLogin(username,password);
//		driver.get("https://www.amazon.com");
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	     System.out.println(pto.Login_confirm.getText());
	     if (pto.Login_confirm.getText().equalsIgnoreCase("Hi, ankita m")) {
	    	 System.out.println("Login Successful");
	     }
	     else {
	    	 System.out.println("Login Unsuccessful");
	     }
	}
	
	@Test(groups="p1", dataProvider= "PhpHotel", dataProviderClass = PhpTravelsDP.class)
    public void Hotel_Search(String Hotel) {
		pto.PhpHotel(Hotel);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		if (pto.Hotel_selection.getText().equalsIgnoreCase("Details")) {
	    	 System.out.println("Hotel Selected");
	     }
	     else {
	    	 System.out.println("Hotel Selection failed");
	     }
	}
	
	@Test(groups="p1", dataProvider= "PhpCheckRoom", dataProviderClass = PhpTravelsDP.class)
	public void BookRoom(String checkin, String checkout){
		pto.PhpCheckRoom(checkin, checkout);
		
	}
	}
	
}
