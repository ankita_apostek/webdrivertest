package com.Gmail.tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import Driver.DriverFactory;

public class Wikipedia extends DriverFactory {
	
	public WebDriver driver;
	public String Url;
	
	public Wikipedia() {
		super("desktop", "chrome");
		this.driver = super.driver;
	}
	
	@BeforeClass(alwaysRun=true)
	public void page(){
		//driver = new WebDriver;
		Url = "https://www.wikipedia.org/";
	}
	
	@Test
	public void language(){
		
		driver.get(Url);
		WebElement language = driver.findElement(By.xpath("//*[@id=\"searchLanguage\"]"));
		System.out.println(language.getText());
	}
		
	@Test(groups="p1")
	public void links(){
		List<WebElement> links = driver.findElements(By.xpath(".//span[@class=\"other-project-title jsl10n\"]"));
		for(WebElement Blink : links) {
			System.out.println(Blink.getText());
		}
				
	}
	
		@AfterClass(alwaysRun=true)
		public void Close(){
			 driver.quit();
		 }
	
	
	
}
