package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AmazonMobileWebPO {
	
	public static final String SingInURL= "https://www.amazon.com/gp/sign-in.html";
	@FindBy (xpath = "//*[@id=\"ap_email_login\"]")  public WebElement Mob_username ;
	@FindBy (xpath = "//*[@id=\"ap_password\"]") public WebElement Mob_password;
	@FindBy (xpath = "//*[@id=\"signInSubmit\"]") public WebElement Mob_Submit;
	@FindBy (xpath = "//*[@id=\"nav-ftr-auth\"]/a") public WebElement Mob_SignOut;
	@FindBy (xpath = "//*[@class=\"nav-input\"][@type=\"text\"]")  public WebElement Mob_SearchBox ;
	@FindBy (xpath = "//*[@class=\"nav-input\"][@type=\"submit\"]") public WebElement Mob_SearchButton;
	@FindBy (xpath = "//*[@id=\"s-slick-result-header\"]") public WebElement Mob_SearchList;
	@FindBy (xpath = "//*[@id=\"resultItems\"]/li[2]/a/div[1]/div[2]/h5/span") public WebElement Mob_SecondItem;
	@FindBy (xpath = "//*[@id=\"sc-buy-box\"]/form/div[1]/span") public WebElement Mob_AddedCart;
	@FindBy (xpath = "//*[@id=\"add-to-cart-button\"]") public WebElement Mob_AddingToCart;
	@FindBy (xpath = "//input[@id='continue']") public WebElement Mob_Email_Continue;
	public WebDriver webdriver;
	
	public AmazonMobileWebPO(WebDriver driver) {
		this.webdriver = driver;
		PageFactory.initElements(webdriver, this);
	}
	
	public void doLoginAmazon(String username, String password) {
		Mob_username.sendKeys(username);
		Mob_Email_Continue.click();
		Mob_password.sendKeys(password);
		Mob_Submit.click();
		}
	public void doSearchAmazon() {
		Mob_SearchBox.click();
		Mob_SearchBox.sendKeys("Core Java books");
		Mob_SearchButton.click();
		}
	
	
}
