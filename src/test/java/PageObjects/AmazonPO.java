package PageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

//import bsh.This;

public class AmazonPO {
	
	public static final String SingInURL= "https://www.amazon.com/gp/sign-in.html";
	@FindBy (xpath = "//input[@id='ap_email']")  public WebElement Text_username ;
	@FindBy (xpath = "//input[@id='ap_password']") public WebElement Text_password;
	@FindBy (xpath = "//input[@id='signInSubmit']") public WebElement Submit;
	@FindBy (xpath = "//*[@id='nav-link-accountList']/span[1]") public WebElement MyAccount;
	@FindBy (css = "#twotabsearchtextbox")  public WebElement SearchBox ;
	@FindBy (xpath = "//*[@id='nav-search-submit-text']") public WebElement SearchButton;
	@FindBy (xpath = "//*[@id='s-result-count']") public WebElement SearchList;
	@FindBy (xpath = "//*[@id='hlb-ptc-btn-native']") public WebElement AddCart;
	@FindBy (xpath = "//*[@id=\\\"result_0\\\"]/div/div/div/div[2]/div[1]/div[1]") public WebElement SecondItem;
	@FindBy (xpath = "//input[@id='add-to-cart-button']") public WebElement AddingToCart;
	@FindBy (xpath = "//input[@id='continue']") public WebElement Email_Continue;
	public WebDriver webdriver;
	
	public AmazonPO(WebDriver driver) {
		this.webdriver = driver;
		PageFactory.initElements(webdriver, this);
	}
	
	public void doLoginAmazon(String username, String password) {
		Text_username.sendKeys(username);
		Email_Continue.click();
		Text_password.sendKeys(password);
		Submit.click();
		}
	public void doSearchAmazon() {
		SearchBox.click();
		SearchBox.sendKeys("Core Java books");
		SearchButton.click();
		}
	
	
}
