package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class PhpTravelsPO {
	
//	public static final String SingInURL= "https://www.phptravels.net/login";
	
	
	@FindBy (xpath = ".//*[@class=\"RTL\"]") public WebElement Login_confirm;
	@FindBy (xpath = ".//input[@name=\"username\"]") public WebElement Lusername;
	@FindBy (xpath = ".//input[@name=\"password\"]") public WebElement Lpassword;
	@FindBy (xpath = ".//*[@type=\"submit\"]") public WebElement Login;
	@FindBy (xpath = "html/body/div[9]/div[4]/div/div[2]/form/div[1]/div/input") public WebElement Checkin;
	@FindBy (xpath = "html/body/div[9]/div[4]/div/div[2]/form/div[2]/div/input") public WebElement Checkout;
	@FindBy (xpath = ".//*[@class=\"panel-heading panel-green hidden-xs\"]") public WebElement Hotel_selection;
	@FindBy (xpath = ".//*[@href=\"https://www.phptravels.net/hotels\"]") public WebElement Hotel_listPage;
	@FindBy (xpath = "//*[contains(@class,'RTL go-text-right mt0 list_title')]") public WebElement Hotel_Names;
	@FindBy (xpath = "//*[@id='body-section']/div[5]/div[3]/div/table/tbody/tr[1]/td") public WebElement Hotel1;
	@FindBy (xpath = "//*[@id='body-section']/div[5]/div[3]/div/table/tbody/tr[2]/td") public WebElement Hotel2;
	@FindBy (xpath = "//*[@id='body-section']/div[5]/div[3]/div/table/tbody/tr[3]/td") public WebElement Hotel3;
	@FindBy (xpath = "//*[@id='body-section']/div[5]/div[3]/div/table/tbody/tr[4]/td") public WebElement Hotel4;
	@FindBy (xpath = "//*[@id='body-section']/div[5]/div[3]/div/table/tbody/tr[5]/td") public WebElement Hotel5;
	@FindBy (xpath = "//*[@id='body-section']/div[5]/div[3]/div/table/tbody/tr[6]/td") public WebElement Hotel6;
	@FindBy (xpath = "//*[@id='body-section']/div[5]/div[3]/div/table/tbody/tr[7]/td") public WebElement Hotel7;
	@FindBy (xpath = "//*[@id='body-section']/div[5]/div[3]/div/table/tbody/tr[8]/td") public WebElement Hotel8;
	
	public WebDriver webdriver;
	
	public PhpTravelsPO(WebDriver driver) {
		this.webdriver = driver;
		PageFactory.initElements(webdriver, this);
	}
	
	public void PhpLogin(String username, String password) {
		Lusername.sendKeys(username);
		Lpassword.sendKeys(password);
		Login.click();
		}
	
	public void PhpGuestRoom(String room, String guests) {
		
		}
	
	public void PhpCheckRoom(String checkin, String checkout) {
		
	}
	
	public void PhpHotel(String Hotel) {
		Hotel_listPage.click();
		if (Hotel.equals("1"))
			Hotel1.click();
		else if (Hotel.equals("2"))
			Hotel2.click();
		else if (Hotel.equals("3"))
			Hotel3.click();
		else if (Hotel.equals("4"))
			Hotel4.click();
		else if (Hotel.equals("5"))
			Hotel5.click();
		else if (Hotel.equals("6"))
			Hotel6.click();
		else if (Hotel.equals("7"))
			Hotel7.click();
		else if (Hotel.equals("8"))
			Hotel8.click();
		else
		System.out.println("No matching Hotel found.");
   }
		
}

